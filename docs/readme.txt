LXALogging Readme
2017.4.17
Jack Sinnott

What is this?
This is a log4net appender implementation. It writes logged events to the REST service maintained by Chakradhar Sunkesula on the Delta team.


How to use this solution:
In general, you will need a reference to log4net.dll & LXALogging, an assembly declartion, a static ILog declaration, and a configuration. 
Prototype as follows:
## begin snippet
	using LXALogging;
	
	// only required in one class per library
	[assembly: log4net.Config.XmlConfigurator(Watch = true)] 

	namespace bar
	{
		class foo
		{
			private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
			
			public void MyMethodtoLog()
			{
				log.Debug("I Log Something()");
			}
		}
	}
## end


Each library will also need <log4net> & <appSettings> sections in its app.config:
## begin snippet
	<?xml version="1.0" encoding="utf-8" ?>
	<configuration>
		<configSections>
			<section name="log4net"
				type="log4net.Config.Log4NetConfigurationSectionHandler, log4net"/>
		</configSections>
		<log4net>
			<appender name="Appender" type="LXALogging.RestLogger, LXALogging">
				<!--layout here-->
			</appender>/>
			<logger name="LXALogging">
				<level value="DEBUG" />
				<appender-ref ref="Appender" />
			</logger>
			<logger name="LXALoggingTests">
				<level value="DEBUG" />
				<appender-ref ref="Appender" />
			</logger>
		</log4net>
		<appSettings>
			<add key ="endpoint" value="http://nrusca-swp4092.nibr.novartis.net:8888/Logger/rest/log/"/>
			<!--change appName value to name of your application-->
			<add key="appName" value="LXALogging"/>
		</appSettings>
	</configuration>
## end

See tests for specific examples. 
