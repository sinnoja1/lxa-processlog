﻿using System;
[assembly: CLSCompliant(true)]

namespace LXA.Extensions
{
	public static class StringExtension
	{
		public static bool IsNotNullOrEmpty(this String str)
		{
			return !String.IsNullOrEmpty(str);
		}

		public static bool IsNotNullOrWhiteSpace(this String str)
		{
			return !String.IsNullOrWhiteSpace(str);
		}
		public static bool IsNullOrEmpty(this String str)
		{
			return String.IsNullOrEmpty(str);
		}

		public static bool IsNullOrWhiteSpace(this String str)
		{
			return String.IsNullOrWhiteSpace(str);
		}

		public static bool ToBool(this String str)
		{
			// can't use Convert.ToBoolean(string) since it only accepts "true" or "false"
			if (
				str != null &&
				(str.ToUpper() == "TRUE" ||
				str.ToUpper() == "T" ||
				str == "1" ||
				str.ToUpper() == "YES" || str.ToUpper() == "Y")
				)
			{ return true; }
			return false;
		}
	}
}
