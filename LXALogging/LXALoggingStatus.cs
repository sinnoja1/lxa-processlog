﻿using System;

namespace LXALogging
{
	public static class LXALoggingStatus
	{
		public static string LastStatusMessage { get; set; }
		public static int LastStatusError { get; set; }
		public static string State { get; set; }

		public static Exception LastException { get; set; }
		private static bool _threwException;
		public static bool ThrewException
		{
			get { return _threwException; }
			set
			{
				_threwException = value;
				if (!value)
				{
					LastException = null;
				}
				else
				{
					if (LastException == null)
					{
						LastException = new Exception();
					}
				}
			}
		}

		public static string SUCCESSSTATE = "Success";
		public static string FAILSTATE = "Fail";
		public static string INPROGREESSSTATE = "In Progress";
	}
}
