﻿using StoryQ;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LXALogging;
using System;
using LXA.Extensions;

namespace LXALoggingTests
{
	[TestClass]
	public class LoggingSuccessTests
    {
		//private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(LoggingTests));
		//private static readonly log4net.Core.ILogger log = log4net.Core.LoggerManager.GetLogger( // typeof(LoggingTests));
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(LoggingSuccessTests));
		
		private string _returnStateValue;
		private string _returnLastMessageValue;
		private int _returnLastErrorValue;
		private bool _returnThrewException;

		[TestMethod]
		public void LXALoggingSuccessTests()
		{
			new Story("LXALoggingSuccessTests")
				.InOrderTo("log events")
				.AsA("system")
				.IWant("to log events via log4net")

					.WithScenario("a logger is provided")
						.Given(AnInitializedLXALoggingSut)
						.When(ILogSomething)
						.Then(LoggerStateIsValid)

					.WithScenario("I can check logging state for success")
						.Given(SomeLoggingSuccessEvent)
							.And(ACleanLoggingReturnEnvironment)
						.When(ICheckTheLoggingSuccessStatus)
						.Then(LXALoggingStatusReturnASuccessStateValue_, LXALoggingStatus.SUCCESSSTATE)
							.And(LXALoggingStatusReturnsASuccessValue_, 0)
							.And(LXALoggingStatusReturnsAnEmptyStatusMessageValue)
							.And(LXALoggingStatusReturnsAThrewExceptionValue_, false)
							.And(LXALoggingStatusReturnsTheAppropriateException)

				.Execute();
		}

		private void LXALoggingStatusReturnsAnEmptyStatusMessageValue()
		{
			Assert.IsTrue(_returnLastMessageValue.IsNullOrEmpty());
		}

		private void LXALoggingStatusReturnsASuccessValue_(int value)
		{
			Assert.AreEqual(value, _returnLastErrorValue);
		}

		private void LXALoggingStatusReturnASuccessStateValue_(string value)
		{
			Assert.AreEqual(value, _returnStateValue);
		}

		private void ICheckTheLoggingSuccessStatus()
		{
			_returnStateValue = LXALoggingStatus.State;
			_returnLastErrorValue = LXALoggingStatus.LastStatusError;
			_returnLastMessageValue = LXALoggingStatus.LastStatusMessage;
			_returnThrewException = LXALoggingStatus.ThrewException;
		}

		private void SomeLoggingSuccessEvent()
		{
			LXALoggingStatus.ThrewException = false;
			LXALoggingStatus.LastStatusError = 0;
			LXALoggingStatus.LastStatusMessage = String.Empty;
			LXALoggingStatus.State = LXALoggingStatus.SUCCESSSTATE;
		}

		private void ACleanLoggingReturnEnvironment()
		{
			_returnLastErrorValue = 0;
			_returnLastMessageValue = String.Empty;
			_returnStateValue = String.Empty;
			_returnThrewException = false;
		}

		private void LXALoggingStatusReturnsTheAppropriateException()
		{
			if (_returnThrewException)
			{
				Assert.IsNotNull(LXALoggingStatus.LastException);
			}
			else
			{
				Assert.IsNull(LXALoggingStatus.LastException);
			}
		}

		private void LXALoggingStatusReturnsAThrewExceptionValue_(bool value)
		{
			Assert.IsTrue(_returnThrewException == value);
		}

		private void AnInitializedLXALoggingSut()
		{
			//no op: initialization handled
			Assert.IsNotNull(log);
		}

		private void ILogSomething()
		{
			log.Debug("I Log Something()");
			//log.Debug(typeof(LoggingTests), "I Log Something()");
			//log.Logger.Log(typeof(LoggingTests), "Blah");
		}

		private void LoggerStateIsValid()
		{
			Assert.IsTrue(LXALoggingStatus.State == LXALoggingStatus.SUCCESSSTATE);
		}
		
	}
}
