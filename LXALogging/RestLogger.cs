﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using log4net.Appender;
using log4net.Core;
using log4net.Util;
using RestSharp;

namespace LXALogging
{
	public class RestLogger : AppenderSkeleton
	{
		//REST Service API - http://nrusca-swp4092.nibr.novartis.net:8888/Logger/api-docs/Logger
		//dev logger - http://nrusca-swd4045.nibr.novartis.net:8888/Logger/rest/log/";
		//prod logger - "http://nrusca-swp4092.nibr.novartis.net:8888/Logger/rest/log/";

		private String _endpoint = ConfigurationManager.AppSettings["endpoint"];//"http://nrusca-swp4092.nibr.novartis.net:8888/Logger/rest/log/";
		private String _appName = ConfigurationManager.AppSettings["appName"]; //"LXALogging";

		private static RestClient _httpclient = new RestClient();
		private string _operationResult;

		public bool Success { get; private set; }

		public RestLogger()
		{
			LogLog.Debug(typeof(string), "ctor RESTAppender");
			UriBuilder uriBuilder = new UriBuilder(_endpoint);
			_httpclient.BaseUrl = new UriBuilder(_endpoint).Uri;
		}

		protected override void Append(LoggingEvent loggingEvent)
		{
			LXALoggingStatus.State = LXALoggingStatus.INPROGREESSSTATE;

			LocationInfo locationInfo = loggingEvent.GetLoggingEventData(FixFlags.LocationInfo).LocationInfo;
			// TODO: test this for split
			String[] stackTraces = { loggingEvent.GetExceptionString() };
			String stackTrace = stackTraces == null ? String.Empty : loggingEvent.GetExceptionString(); // Joiner.on(", ").join(stackTraces);

			string hostName = getHostName();
			string iP = getIP();

			Dictionary<string, object> info = new Dictionary<string, object>();
			info.Add("appName", _appName);
			info.Add("hostIP", iP == null ? String.Empty : iP);
			info.Add("hostName", hostName == null ? String.Empty : hostName);
			info.Add("loggerName", loggingEvent.LoggerName);

			if (locationInfo != null)
			{
				info.Add("fileName", locationInfo.FileName != null ? locationInfo.FileName : string.Empty);
				info.Add("className", locationInfo.ClassName != null ? locationInfo.ClassName : string.Empty);
				info.Add("methodName", locationInfo.MethodName != null ? locationInfo.MethodName : string.Empty);
				info.Add("lineNumber", locationInfo.LineNumber != null ? Int32.Parse(locationInfo.LineNumber) : 0);
			}
			else
			{
				info.Add("fileName", string.Empty);
				info.Add("className", string.Empty);
				info.Add("methodName", string.Empty);
				info.Add("lineNumber", 0);
			}

			info.Add("level", loggingEvent.Level.ToString() == null ? String.Empty : loggingEvent.Level.ToString());
			info.Add("message", loggingEvent.RenderedMessage == null ? String.Empty : loggingEvent.RenderedMessage);
			info.Add("appStartTime", LoggingEvent.StartTime.ToString("yyyy-MM-ddTHH:mm:ssK"));
			info.Add("threadName", loggingEvent.ThreadName == null ? String.Empty : loggingEvent.ThreadName);
			info.Add("ndc", loggingEvent.Domain == null ? "" : loggingEvent.Domain);
			info.Add("stamp", loggingEvent.TimeStamp.ToString("yyyy-MM-ddTHH:mm:ssK"));
			info.Add("stackTrace", stackTrace == null ? "" : stackTrace);

			RestRequest request = new RestRequest(Method.POST);
			request.AddHeader("Content-Type", "application/json");
			request.AddJsonBody(info);

			try
			{
				IRestResponse response = _httpclient.Execute<Rootobject>(request);
				_operationResult = response.StatusCode == System.Net.HttpStatusCode.OK ? response.Content : response.ErrorMessage;
				Success = response.StatusCode == System.Net.HttpStatusCode.Accepted
					|| response.StatusCode == System.Net.HttpStatusCode.OK;

				if (!Success)
				{
					LogLocal(String.Format("{2}\r\nLogging failure: \r\nResponse.Content: {0}\r\nResponse.ErrorMessage: {1}\r\nRequest.Body: {3}\r\n", response.Content, response.ErrorMessage, DateTime.Now.ToString(), request.Parameters[1].Value));
					LXALoggingStatus.State = LXALoggingStatus.FAILSTATE;
					LXALoggingStatus.LastStatusMessage = _operationResult;
					LXALoggingStatus.LastStatusError = response.ErrorException != null ? -1 : 0;
					LXALoggingStatus.ThrewException = response.ErrorException != null ? true : false;
					LXALoggingStatus.LastException = response.ErrorException;
				}
				else
				{
					LXALoggingStatus.State = LXALoggingStatus.SUCCESSSTATE;
					LXALoggingStatus.LastStatusMessage = _operationResult;
					LXALoggingStatus.LastStatusError = 0;
					LXALoggingStatus.ThrewException = false;
				}
			}
			catch (Exception e)
			{
				_operationResult = String.Format("Webservice call to {0} failed: {1} {2}", _endpoint, e.Message, e.Data != null ? e.Data.ToString() : "");
				Success = false;
				LogLocal(String.Format("{1}\r\nLogging exception:\r\n{0}", _operationResult, DateTime.Now.ToString()));
			}
		}

		private void LogLocal(string v)
		{
#if DEBUG
			System.IO.File.WriteAllText(Guid.NewGuid().ToString() + ".log", v);
#endif
		}

		private string getHostName()
		{
			return Dns.GetHostName();
		}

		private string getIP()
		{
			var host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (var ip in host.AddressList)
			{
				if (ip.AddressFamily == AddressFamily.InterNetwork)
				{
					return ip.ToString();
				}
			}
			throw new Exception("Local IP Address Not Found!");
		}

		// implement if custom logger using configurable messages is needed
		// see: https://logging.apache.org/log4net/log4net-1.2.13/release/sdk/log4net.Layout.html
		//public bool requiresLayout()
		//{
		//	return true;
		//}

		internal class Rootobject
		{
			public string appName { get; set; }
			public string hostIP { get; set; }
			public string hostName { get; set; }
			public string loggerName { get; set; }
			public string fileName { get; set; }
			public string className { get; set; }
			public string methodName { get; set; }
			public int lineNumber { get; set; }
			public string level { get; set; }
			public string message { get; set; }
			public DateTime appStartTime { get; set; }
			public string threadName { get; set; }
			public string ndc { get; set; }
			public DateTime stamp { get; set; }
			public string stackTrace { get; set; }
		}

	}
}
