# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* What is this?
* This is a log4net appender implementation. It writes logged events to 
* the REST service maintained by Chakradhar Sunkesula on the Delta team.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* The solution code is self contained.
* Referenced libraries that are not in NuGet are included in the Libs folder.
* Please see the readme for how to add logging to your application.
* Deployment is via DLL. Whether we create a NuGet repos or use BitBucket for this is TBD.

### Contribution guidelines ###

* Please use StoryQ with MSTest.
* Code review will be ad hoc.
* Please follow standard VS formatting and naming conventions for C#. Resharper is not necessary.

### Who do I talk to? ###

* See Jack Sinnott or Erik Hesse for project info.