﻿using StoryQ;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LXALogging;
using System;
using LXA.Extensions;

namespace LXALoggingTests
{
	[TestClass]
	public class LoggingFailureTests
	{
		//private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(LoggingTests));
		//private static readonly log4net.Core.ILogger log = log4net.Core.LoggerManager.GetLogger( // typeof(LoggingTests));
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		private string _returnStateValue;
		private string _returnLastMessageValue;
		private int _returnLastErrorValue;
		private bool _returnThrewException;

		[TestMethod]
		public void LXALoggingFailureTests()
		{
			new Story("LXALoggingFailureTests")
				.InOrderTo("log events")
				.AsA("system")
				.IWant("to log events via log4net")

					.WithScenario("I can check logging state for errors")
						.Given(SomeLoggingErrorEvent)
							.And(ACleanLoggingReturnEnvironment)
						.When(ICheckTheLoggingErrorStatus)
						.Then(LXALoggingStatusReturnsAStateValue)
							.And(LXALoggingStatusReturnsAnErrorValue_, 1)
							.And(LXALoggingStatusReturnsAStatusMessageValue)
							.And(LXALoggingStatusReturnsAThrewExceptionValue_, true)
							.And(LXALoggingStatusReturnsTheAppropriateException)

				.Execute();
		}

		private void ACleanLoggingReturnEnvironment()
		{
			_returnLastErrorValue = 0;
			_returnLastMessageValue = String.Empty;
			_returnStateValue = String.Empty;
			_returnThrewException = false;
		}

		private void LXALoggingStatusReturnsTheAppropriateException()
		{
			if (_returnThrewException)
			{
				Assert.IsNotNull(LXALoggingStatus.LastException);
			}
			else
			{
				Assert.IsNull(LXALoggingStatus.LastException);
			}
		}

		private void LXALoggingStatusReturnsAThrewExceptionValue_(bool value)
		{
			Assert.IsTrue(_returnThrewException == value);
		}

		private void LXALoggingStatusReturnsAStatusMessageValue()
		{
			Assert.IsTrue(_returnLastMessageValue.IsNotNullOrEmpty());
		}

		private void LXALoggingStatusReturnsAnErrorValue_(int value)
		{
			Assert.IsTrue(_returnLastErrorValue == value);
		}

		private void LXALoggingStatusReturnsAStateValue()
		{
			Assert.IsTrue(_returnStateValue.IsNotNullOrEmpty());

			_returnStateValue = String.Empty;
		}

		private void ICheckTheLoggingErrorStatus()
		{
			_returnStateValue = LXALoggingStatus.State;
			_returnLastErrorValue = LXALoggingStatus.LastStatusError;
			_returnLastMessageValue = LXALoggingStatus.LastStatusMessage;
			_returnThrewException = LXALoggingStatus.ThrewException;
		}

		private void SomeLoggingErrorEvent()
		{
			LXALoggingStatus.LastStatusError = 1;
			LXALoggingStatus.LastStatusMessage = "error message";
			LXALoggingStatus.ThrewException = true;
			LXALoggingStatus.State = LXALoggingStatus.FAILSTATE;
		}		

	}
}
