﻿using System;
using StoryQ;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LXA.Extensions;

namespace LXA.ExtensionsTests
{
	[TestClass]
	public class StringExtensionTests
    {	
		string _sut = String.Empty;
		bool _state;

		[TestMethod]
		public void WeHaveExtendedStringMethods()
		{
			new Story("we have extended string methods")
				.InOrderTo("more easily check string state")
				.AsA("system")
				.IWant("a set of string class extension methods")

						.WithScenario("I can check for not white space")
							.Given(AStringSut)
							.When(ICheckIfSut_IsNotNullOrWhitespace, "bob")
							.Then(TheSutIs_, true)

						.WithScenario("I can check for not empty")
							.Given(AStringSut)
							.When(ICheckIfSut_IsNotNullOrEmpty, "beth")
							.Then(TheSutIs_, true)

						.WithScenario("I can check for white space")
							.Given(AStringSut)
							.When(ICheckIfSut_IsNullOrWhitespaceUsingSutIsNullOrWhiteSpaceExtensionMethod, " ")
							.Then(TheSutIs_, true)

						.WithScenario("I can check for empty")
							.Given(AStringSut)
							.When(ICheckIfEmptySut_IsNullOrEmptyUsingSutIsNullOrEmptyExtensionMethod, "")
							.Then(TheSutIs_, true)

						.WithScenario("I can convert string value equivalent of true to bool")
							.Given(AStringSut)
							.When(ICheckIfSutValueTRUET1YESYIsTrueUsingSutToBoolExtensionMethod)
							.Then(TheSutIs_, true)

						.WithScenario("I can convert string value equivalent of not true to bool")
							.Given(AStringSut)
							.When(ICheckIfSutValueNotTRUET1YESYIsTrueUsingSutToBoolExtensionMethod)
							.Then(TheSutIs_, false)
				.Execute();
		}

		private void AStringSut()
		{
			// no op
		}

		private void ICheckIfSut_IsNotNullOrWhitespace(string value)
		{
			_state = value.IsNotNullOrWhiteSpace();
		}

		private void TheSutIs_(bool state)
		{
			Assert.IsTrue(_state == state);
		}

		private void ICheckIfSut_IsNotNullOrEmpty(string value)
		{
			_state = value.IsNotNullOrEmpty();
		}

		private void ICheckIfSut_IsNullOrWhitespaceUsingSutIsNullOrWhiteSpaceExtensionMethod(string value)
		{
			_state = value.IsNullOrWhiteSpace();
		}

		private void ICheckIfEmptySut_IsNullOrEmptyUsingSutIsNullOrEmptyExtensionMethod(string value)
		{
			_state = value.IsNullOrEmpty();
		}

		private void ICheckIfSutValueTRUET1YESYIsTrueUsingSutToBoolExtensionMethod()
		{
			_state = true;
			string[] a = { "TRUE", "T", "1", "YES", "Y" };
			foreach (string b in a)
			{
				_state = (b.ToBool() && _state) ? true : false;
			}
		}

		private void ICheckIfSutValueNotTRUET1YESYIsTrueUsingSutToBoolExtensionMethod()
		{
			string str = "bobble";
			_state = str.ToBool();
		}
	}		
}
